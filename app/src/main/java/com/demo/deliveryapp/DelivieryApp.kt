package com.demo.deliveryapp

import android.app.Application
import com.demo.deliveryapp.di.appModules
import com.demo.deliveryapp.di.remoteDataSourceModules
import org.koin.ContextCallback
import org.koin.android.ext.android.startKoin
import org.koin.standalone.StandAloneContext
import timber.log.Timber

class DelivieryApp :Application() {
    override fun onCreate() {
        super.onCreate()
    setupDependencyInjection()
    }

    private fun setupDependencyInjection() {

        // Start Koin context
        startKoin(this, appModules + remoteDataSourceModules)

        // Listen with ContextCallback
        StandAloneContext.registerContextCallBack(object : ContextCallback {
            // Notified on context dropped
            override fun onContextReleased(contextName: String) {
                Timber.d("Context $contextName has been dropped")
            }
        })
    }

}