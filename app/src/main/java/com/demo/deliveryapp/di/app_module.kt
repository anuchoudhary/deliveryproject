package com.demo.deliveryapp.di


import com.demo.deliveryapp.storage.DeliveryCache
import com.demo.deliveryapp.ui.delivery.DeliveryListPresenter
import com.demo.deliveryapp.ui.delivery.MainContract
import com.demo.deliveryapp.ui.delivery.MainPresenter
import com.demo.deliveryapp.util.MAIN_VIEW
import com.demo.deliveryapp.util.rx.AppSchedulers
import com.demo.deliveryapp.util.rx.RxSchedulers
import com.github.ajalt.timberkt.Timber
import org.koin.dsl.module.applicationContext


val presenterModule = applicationContext {
    factory { params -> MainPresenter(params[MAIN_VIEW], get(),get(),get(),
        DeliveryListPresenter(),get()) as MainContract.Presenter }
}

val cacheModule = applicationContext {
    bean { DeliveryCache(get()) }
            .also { Timber.w { "[KOIN] Built Cache Module" } }
}





val rxModule = applicationContext {
    bean { AppSchedulers() as RxSchedulers }
            .also { Timber.w { "[KOIN] Built Schedulers Module" } }
}

val appModules = listOf(presenterModule, rxModule,   cacheModule)
