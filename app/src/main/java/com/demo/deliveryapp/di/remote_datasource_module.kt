package com.demo.deliveryapp.di

import com.demo.deliveryapp.BuildConfig
import com.demo.deliveryapp.network.ConnectionState
import com.demo.deliveryapp.network.DeliveryWebService
import com.demo.deliveryapp.util.BASE_URL

import com.google.gson.GsonBuilder
import okhttp3.Cache
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module.applicationContext
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

import java.io.File
import java.util.concurrent.TimeUnit


val remoteDataSourceModuleDelivery = applicationContext {
    bean { createHttpLogger() }
    bean { createWebService<DeliveryWebService>(createOkHttpClient(get()), BASE_URL) }
}



val connectionStateModule = applicationContext {
    bean { ConnectionState(get()) }
}

fun createHttpLogger(): Interceptor {

    val logLevel = if (BuildConfig.DEBUG) {
        HttpLoggingInterceptor.Level.BASIC
    } else {
        HttpLoggingInterceptor.Level.NONE
    }

    val httpLoggingInterceptor = HttpLoggingInterceptor(HttpLoggingInterceptor.Logger {
        //Timber.tag("Delivery Network").d("[NETWORK] $it")
    })

    httpLoggingInterceptor.level = logLevel

    return httpLoggingInterceptor
}

// TODO: Potentially replace or remove if not required
fun createCache(cacheDir: File): Cache {
    val cacheSize: Long = 10 * 1024 * 1024 // 10MB
    return Cache(cacheDir, cacheSize)
}

fun createOkHttpClient(interceptor: Interceptor): OkHttpClient {
    return OkHttpClient.Builder()
//            .addInterceptor { chain ->
//                val builder = chain
//                        .request()
//                        .newBuilder()
//                if (isJsonContent) {
//                    builder.addHeader("accept", "application/json")
//                    builder.addHeader("Content-Type", "application/json")
//                }
//                val customerToken = preferences.getUsersToken()
//
//                if (customerToken != null) {
//                    builder.addHeader(X_CUSTOMER_TOKEN, customerToken)
//                }
//
//                val request = builder
//                        .addHeader(X_CONSUMER_TOKEN, xToken)
//                        .build()
//                chain.proceed(request)
//            }
            .connectTimeout(60L, TimeUnit.SECONDS)
            .readTimeout(60L, TimeUnit.SECONDS)
            .addNetworkInterceptor(interceptor).build()
}

inline fun <reified T> createWebService(okHttpClient: OkHttpClient, url: String): T {
    val gson = GsonBuilder()
            .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")//SEEMS LIKE THEY  keep changing formats
            .create()
    val retrofit = Retrofit.Builder()
            .baseUrl(url)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create()).build()
    return retrofit.create(T::class.java)
}

val remoteDataSourceModules = listOf(remoteDataSourceModuleDelivery, connectionStateModule)