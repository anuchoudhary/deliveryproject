package com.demo.deliveryapp.model

data class Delivery(val id:String,
                    val description:String,
                    val imageUrl:String,
                    val location:Location)