package com.demo.deliveryapp.model

data class Location(val lat:Double,val lng:Double,val address:String="")