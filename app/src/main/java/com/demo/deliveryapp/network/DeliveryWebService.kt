package com.demo.deliveryapp.network

import com.demo.deliveryapp.model.Delivery
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.GET

interface DeliveryWebService {
    @GET("deliveries")
    fun getDelivery(): Observable<Response<List<Delivery>>>
}