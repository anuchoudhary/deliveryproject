package com.demo.deliveryapp.storage

import androidx.room.*

@Dao
interface BaseDao<in T> {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(t: T)

    @Delete
    fun delete(type: T)

    @Update
    fun update(type: T)
}