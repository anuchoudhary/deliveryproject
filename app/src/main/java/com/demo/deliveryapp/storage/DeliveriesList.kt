package com.demo.deliveryapp.storage

import com.demo.deliveryapp.model.Delivery

data class DeliveriesList(
        val deliveriesList: List<Delivery>
)