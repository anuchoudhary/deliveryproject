package com.demo.deliveryapp.storage

import android.content.Context
import android.location.Location
import androidx.room.Room
import com.demo.deliveryapp.model.Delivery
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.reactivex.Completable
import io.reactivex.Single

class DeliveryCache(context: Context) {
    private val database =
        Room.databaseBuilder(context, RequestResponseDatabase::class.java, DATABASE_NAME).build()
    private val gson = Gson()

    fun clear(): Completable = Completable.fromAction { database.requestResponseDao().clear() }

    fun getResponseForRequest(key: String): Single<DeliveriesList> {
        return Single.fromCallable {
            database.requestResponseDao().getResponseForId(key)
                ?: DeliveryRequestResponseModel(0,"")
        }.map { fromRequestResponseModelToDeliveryList(it) }
    }

    private fun fromRequestResponseModelToDeliveryList(model: DeliveryRequestResponseModel): DeliveriesList {
        val itemType = object : TypeToken<List<Delivery>>() {}.type
        return when {
            model.response.isEmpty() -> DeliveriesList(emptyList())
            else -> {
                DeliveriesList(gson.fromJson<List<Delivery>>(model.response, itemType))
            }

        }
    }


    fun add(
        deliveriesList: List<Delivery>): Completable {
        val json = gson.toJson(deliveriesList)
        return Completable.fromAction {
            database.requestResponseDao().clear()
            database.requestResponseDao().insert(
                DeliveryRequestResponseModel
                    (0, json)
            )
        }
    }

    private fun addNew(
        value: String
    ): Completable {
        return Completable.fromAction { database.requestResponseDao().clear() }
            .andThen(Completable.fromAction {
                database.requestResponseDao()
                    .insert(DeliveryRequestResponseModel(0,value))
            })
    }

    companion object {
        const val DATABASE_NAME = "request_response_"
        fun convertParamsToString(
            id: String
        ): String {
            return "$id"
        }
    }
}