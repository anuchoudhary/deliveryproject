package com.demo.deliveryapp.storage

import androidx.room.Dao
import androidx.room.Query

@Dao
interface DeliveryRequestResponseDao : BaseDao<DeliveryRequestResponseModel> {

    @Query(value = "SELECT * FROM requestDelivery WHERE id = :id")
    fun getResponseForId(id: String): DeliveryRequestResponseModel?

    @Query("DELETE FROM requestDelivery")
    fun clear()

}