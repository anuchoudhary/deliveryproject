package com.demo.deliveryapp.storage

import android.location.Location
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "requestDelivery")
data class DeliveryRequestResponseModel(

        @PrimaryKey(autoGenerate = true)
        @ColumnInfo(name = "id")
        val id: Int?=null,

        @ColumnInfo(name = "response")
        val response: String
)