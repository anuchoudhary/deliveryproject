package com.demo.deliveryapp.storage

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [DeliveryRequestResponseModel::class], version = 1, exportSchema = false)
abstract class RequestResponseDatabase : RoomDatabase() {
    abstract fun requestResponseDao(): DeliveryRequestResponseDao
}