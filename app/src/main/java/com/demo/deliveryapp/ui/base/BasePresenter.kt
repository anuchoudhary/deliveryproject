package com.demo.deliveryapp.ui.base

interface BasePresenter<V> {
    fun start()
    fun stop()
    var view: V
}
