package com.demo.deliveryapp.ui.base

interface BaseView<out P : BasePresenter<*>> {
    val presenter: P
}