package com.demo.deliveryapp.ui.main.discover

interface DeliveryListItemView {

    fun setImageUrl(url: String?)
    fun setAddressLabel(address: String)
    fun setDescriptionLabel(description: String)
    fun setOnClickListener(DeliveryListClickListener: DeliveryListOnClickListener)

}