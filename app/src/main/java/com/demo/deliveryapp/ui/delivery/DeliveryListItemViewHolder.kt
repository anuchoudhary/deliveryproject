package com.demo.deliveryapp.ui.main.discover

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.demo.deliveryapp.R
import com.demo.deliveryapp.util.loadImage
import kotlinx.android.synthetic.main.delivery_item.view.*


class DeliveryListItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), DeliveryListItemView {

    private var rootView:androidx.cardview.widget.CardView  = itemView.root_view
    private var imageView: ImageView = itemView.deliveryImage
    private var descriptionLabel: TextView = itemView.deliveryDescription
    private var locationLabel: TextView = itemView.deliveryAddress

    override fun setImageUrl(url: String?){
      imageView.loadImage(url, R.drawable.no_image, R.drawable.no_image)
    }

    override fun setDescriptionLabel(description: String) {
        descriptionLabel.text = description
    }

    override fun setAddressLabel(address: String) {
        locationLabel.text = address
    }

    override fun setOnClickListener(deliveryListClickListener: DeliveryListOnClickListener) {
        rootView.setOnClickListener { deliveryListClickListener.clicked() }
    }
}