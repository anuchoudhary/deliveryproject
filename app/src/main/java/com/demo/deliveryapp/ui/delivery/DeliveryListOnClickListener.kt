package com.demo.deliveryapp.ui.main.discover

abstract class DeliveryListOnClickListener {

    abstract fun clicked()

}