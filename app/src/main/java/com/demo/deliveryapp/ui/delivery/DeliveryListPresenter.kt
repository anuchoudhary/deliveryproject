package com.demo.deliveryapp.ui.delivery

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.demo.deliveryapp.R
import com.demo.deliveryapp.model.Delivery
import com.demo.deliveryapp.ui.main.discover.DeliveryListItemView
import com.demo.deliveryapp.ui.main.discover.DeliveryListItemViewHolder
import com.demo.deliveryapp.ui.main.discover.DeliveryListOnClickListener
import com.demo.deliveryapp.ui.map.MapsActivity


class DeliveryListPresenter(){
    private var displayNoResultsEnabled = false
    private var deliveries: List<Delivery> = listOf()
    var context:Context?=null
    val adapter: DeliveryListRecyclerViewAdapter = DeliveryListRecyclerViewAdapter(this)
    val DeliveryCount: Int
        get() = deliveries.size


    class DeliveryListRecyclerViewAdapter(val presenter: DeliveryListPresenter) :
        RecyclerView.Adapter<RecyclerView.ViewHolder>() {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
            return DeliveryListItemViewHolder(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.delivery_item, parent, false)
            )
        }

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
            presenter.onBindRepositoryRowViewAtPosition(position, holder)
        }

        override fun getItemCount(): Int = presenter.DeliveryCount

    }

    fun onBindRepositoryRowViewAtPosition(position: Int, itemView: Any) {
        with(itemView as DeliveryListItemView) {
            if (deliveries.size > 0 && deliveries.size > position) {
                val delivery = deliveries[position]
                setImageUrl(delivery.imageUrl)
                setDescriptionLabel(delivery.description)
                setAddressLabel(delivery.location.address)
                setOnClickListener(object : DeliveryListOnClickListener() {
                    override fun clicked() {
                        MapsActivity.startAsModal(
                            context =context,
                            id=delivery.id,
                            delivery = delivery
                        )
                    }
                })

            }
        }
    }


    fun enableNoResultsMessage() {
        displayNoResultsEnabled = true
        adapter.notifyDataSetChanged()
    }

    fun applyDeliveries(deliveries: List<Delivery>?, from: Int) {
        val r = deliveries ?: listOf()
        if (from == 0) this.deliveries = listOf()
        if (from > 0) this.deliveries =
            this.deliveries.slice(0 until Math.max(from, this.deliveries.size))
        this.deliveries = this.deliveries + r
    }

}
