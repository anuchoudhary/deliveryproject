package com.demo.deliveryapp.ui.delivery

import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.animation.TranslateAnimation
import androidx.core.view.isInvisible
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.demo.deliveryapp.R
import com.demo.deliveryapp.ui.base.BaseActivity
import com.demo.deliveryapp.util.MAIN_VIEW
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.snackbar.*
import org.koin.android.ext.android.inject

class MainActivity : BaseActivity(), MainContract.View {

    private var snackbarHeight = -1f

    override fun showOfflinePopup() {
        if (snackbar.isInvisible) {
            val anim = TranslateAnimation(0F, 0F, snackbarHeight, 0f)
            anim.duration = 600
            anim.fillAfter = true
            snackbar_container.visibility = View.VISIBLE
            snackbar.visibility = View.VISIBLE
            snackbar.animation = anim
        }
    }

    override fun hideOfflinePopup() {
        if (snackbar.isVisible) {
            val anim = TranslateAnimation(0F, 0F, 0f, snackbarHeight)
            anim.duration = 400
            anim.fillAfter = true
            snackbar.visibility = View.INVISIBLE
            snackbar.animation = anim
            snackbar_container.visibility = View.GONE
        }
    }

    override val presenter: MainContract.Presenter by inject { mapOf(MAIN_VIEW to this) }

    override val layoutResource: Int
        get() = R.layout.activity_main

    override fun startPresenter() {
        presenter.start()
    }

    override fun stopPresenter() {
        presenter.stop()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutResource)
        setupView()
    }

    override fun showLoading() {
        loading_bar.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        loading_bar.visibility = View.GONE
    }

    override fun hideExternalLoader() {
        pull_to_refresh.isRefreshing = false
    }

    override fun getApplicationContext():Context {
        return this
    }
    override fun scrollToTop() {
        delivery_recycler_view.post { delivery_recycler_view.smoothScrollToPosition(0) }
    }

    private fun setupView() {
        back_button.setOnClickListener({
            super.onBackPressed()
        })
        pull_to_refresh.setOnRefreshListener { presenter.refreshFromServer() }
        setupRecyclerView(delivery_recycler_view)
    }

    private fun setupRecyclerView(recyclerView: RecyclerView) {
        recyclerView.layoutManager = LinearLayoutManager(
            recyclerView.context, LinearLayoutManager.VERTICAL, false)
        recyclerView.adapter = presenter.adapter
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (dy < 0) return
                val layoutManager = recyclerView.layoutManager as LinearLayoutManager
                val totalItemCount = layoutManager.itemCount
                val lastVisible = layoutManager.findLastVisibleItemPosition()
            }
        }
        )
    }
}
