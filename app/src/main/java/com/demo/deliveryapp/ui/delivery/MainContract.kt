package com.demo.deliveryapp.ui.delivery

import android.content.Context
import android.graphics.Bitmap
import androidx.recyclerview.widget.RecyclerView
import com.demo.deliveryapp.ui.base.BasePresenter
import com.demo.deliveryapp.ui.base.BaseView


interface MainContract {
    interface View : BaseView<Presenter> {
        fun showOfflinePopup()
        fun hideOfflinePopup()
        fun showLoading()
        fun hideLoading()
        fun scrollToTop()
        fun hideExternalLoader()
        fun getApplicationContext():Context

    }

    interface Presenter : BasePresenter<View> {
        fun handleOfflinePopup(appContext: Context)
        fun refreshFromServer()
        val adapter:RecyclerView.Adapter<RecyclerView.ViewHolder>
    }
}