package com.demo.deliveryapp.ui.delivery

import android.content.Context
import android.net.NetworkInfo
import androidx.recyclerview.widget.RecyclerView
import com.demo.deliveryapp.model.Delivery
import com.demo.deliveryapp.network.ConnectionState
import com.demo.deliveryapp.network.DeliveryWebService
import com.demo.deliveryapp.storage.DeliveriesList
import com.demo.deliveryapp.storage.DeliveryCache
import com.demo.deliveryapp.ui.base.AbstractPresenter
import com.demo.deliveryapp.util.rx.RxSchedulers
import com.demo.deliveryapp.util.rx.applyUiSchedulers
import com.github.pwittchen.reactivenetwork.library.rx2.Connectivity
import com.github.pwittchen.reactivenetwork.library.rx2.ReactiveNetwork
import retrofit2.Response

class MainPresenter(
    override var view: MainContract.View,
    private val schedulers: RxSchedulers,
    private val deliveryCache: DeliveryCache,
    private val connectionState: ConnectionState,
    private val listPresenter: DeliveryListPresenter,
    private val deliveryWebService: DeliveryWebService
) : AbstractPresenter<MainContract.View, MainContract.Presenter>(), MainContract.Presenter {
    private var requestIndex = 0
    private var isLoading: Boolean = false

    override fun refreshFromServer() =
        getListFromServer(hideLoader = true, hideExternalLoaderAfterFinished = true)

    override val adapter: RecyclerView.Adapter<RecyclerView.ViewHolder> = listPresenter.adapter
    override fun handleOfflinePopup(appContext: Context) {
        addDisposable {
            ReactiveNetwork
                .observeNetworkConnectivity(appContext)
                .applyUiSchedulers(schedulers)
                .subscribe(this::onSuccess) { it.printStackTrace() }
        }
    }

    override fun start() {
        listPresenter.context=view.getApplicationContext()
        tryToDisplayOfflineResults()
    }

    private fun onSuccess(connectivity: Connectivity) {
        when (connectivity.state()) {
            NetworkInfo.State.CONNECTED -> view.hideOfflinePopup()
            NetworkInfo.State.DISCONNECTED -> view.showOfflinePopup()
            else -> {
            }
        }
    }

    private fun tryToDisplayOfflineResults() {
        val stringParams = DeliveryCache.convertParamsToString(id = "0")
        var listOfDeliveries = DeliveriesList(emptyList())
        addDisposable {
            deliveryCache.getResponseForRequest(stringParams)
                .map { listOfDeliveries = applyInitialOfflineList(it, 0) }
                .applyUiSchedulers(schedulers)
                .subscribe({ continueStartWithOfflineResults(listOfDeliveries) },
                    { continueStartWithOfflineResults(listOfDeliveries) })
        }

    }

    private fun applyInitialOfflineList(
        listOfDeliveries: DeliveriesList,
        from: Int
    ): DeliveriesList {
        val list = listOfDeliveries.deliveriesList
        if (list.isNotEmpty() && from == 0) {
            listPresenter.applyDeliveries(list, from)
        }
        return listOfDeliveries
    }

    private fun continueStartWithOfflineResults(delivery: DeliveriesList) {
        if (delivery.deliveriesList.isEmpty() && connectionState.isNetworkAvailable()) {
            getListFromServer()
        } else if (delivery.deliveriesList.isNotEmpty() && connectionState.isNetworkAvailable()) {
            adapter.notifyDataSetChanged()
            getListFromServer(hideLoader = true)
        } else {
            listPresenter.enableNoResultsMessage()
            adapter.notifyDataSetChanged()
        }
    }

    private fun getListFromServer(
        from: Int = 0,
        hideLoader: Boolean = false,
        hideExternalLoaderAfterFinished: Boolean = false
    ) {
        isLoading = true
        if (!hideLoader) {
            view.showLoading()
        }
        var backupOfflineList = emptyList<Delivery>()

        addDisposable {
            deliveryCache.getResponseForRequest("")
                .map { backupOfflineList = applyInitialOfflineList(it, 0).deliveriesList }
                .applyUiSchedulers(schedulers)
                .subscribe({
                    addDisposable {
                        deliveryWebService.getDelivery()
                            .applyUiSchedulers(schedulers).subscribe(
                                { onSuccess(it, 0, hideLoader, hideExternalLoaderAfterFinished) },
                                {
                                    onError(
                                        it,
                                        backupOfflineList,
                                        hideLoader,
                                        hideExternalLoaderAfterFinished
                                    )
                                })
                    }
                }, { it.printStackTrace() })
        }
    }


    private fun onSuccess(
        response: Response<List<Delivery>>?,
        from: Int = 0,
        hideLoader: Boolean,
        hideExternalLoaderAfterFinished: Boolean
    ) {
        if (!hideLoader) {
            hideLoading()
        }
        if (hideExternalLoaderAfterFinished) {
            view.hideExternalLoader()
        }
        isLoading = false
        val total = response?.headers()?.get("x-total")?.toInt()
        listPresenter.enableNoResultsMessage()

        listPresenter.applyDeliveries(response?.body(), from)
        adapter.notifyDataSetChanged()

        (response?.body())?.let {
            addDisposable {
                deliveryCache.add(it).subscribeOn(schedulers.io).subscribe()
            }
        }
        if (from == 0) view.scrollToTop()

        when (response?.code()) {
            200 -> null
            // in 500..599 -> view.showServerError()
            //else -> view.showGeneralNetworkError()
        }
    }


    private fun onError(
        throwable: Throwable,
        backupList: List<Delivery>? = null,
        hideLoader: Boolean,
        hideExternalLoaderAfterFinished: Boolean
    ) {
        listPresenter.enableNoResultsMessage()
        if (!hideLoader) {
            hideLoading()
        }

        if (hideExternalLoaderAfterFinished) {
            view.hideExternalLoader()
        }
        isLoading = false

        if (backupList != null && backupList.isNotEmpty()) {
            listPresenter.applyDeliveries(backupList, 0)
            adapter.notifyDataSetChanged()
        }
//        if (!fromPaging)
//            when (throwable) {
//                is UnknownHostException -> view.showNoConnectionError()
//                is SocketTimeoutException -> view.showTimeoutError()
//            }

        throwable.printStackTrace()
    }

    private fun showLoading() {
        requestIndex++
        view.showLoading()
    }

    private fun hideLoading() {
        requestIndex--
        if (requestIndex == 0) {
            view.hideLoading()
        } else if (requestIndex < 0) {//need implementaion here
        }
    }

}

