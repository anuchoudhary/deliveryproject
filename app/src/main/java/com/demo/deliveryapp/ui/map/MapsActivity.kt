package com.demo.deliveryapp.ui.map

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.FragmentActivity
import com.demo.deliveryapp.R
import com.demo.deliveryapp.model.Delivery
import com.demo.deliveryapp.util.DELIVERY_DATA
import com.demo.deliveryapp.util.DELIVERY_ID
import com.demo.deliveryapp.util.loadImage
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_maps.*

class MapsActivity : FragmentActivity(), OnMapReadyCallback {
    private var mMap: GoogleMap? = null
    lateinit var behavior: BottomSheetBehavior<ConstraintLayout>

    var delivery: Delivery? = null
    private val gson = Gson()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        if (intent != null) {
            delivery = gson.fromJson<Any>(
                intent.getStringExtra(DELIVERY_DATA),
                Delivery::class.java
            ) as Delivery
            updateDetials(delivery!!)

        }

        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment!!.getMapAsync(this)

        behavior = BottomSheetBehavior.from(bottomSheet)

        closeButtonSheet.setOnClickListener {
            if (behavior.state == BottomSheetBehavior.STATE_EXPANDED) {
                behavior.state = BottomSheetBehavior.STATE_COLLAPSED
            } else if (behavior.state == BottomSheetBehavior.STATE_COLLAPSED) {
                behavior.state = BottomSheetBehavior.STATE_EXPANDED
            }
        }

        behavior.addBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                // React to state change
                when (newState) {
                    BottomSheetBehavior.STATE_HIDDEN -> {
                    }
                    BottomSheetBehavior.STATE_EXPANDED -> {
                        closeButtonSheet.setImageResource(android.R.drawable.arrow_down_float)
                    }
                    BottomSheetBehavior.STATE_COLLAPSED -> {
                        closeButtonSheet.setImageResource(android.R.drawable.arrow_up_float)
                    }
                    BottomSheetBehavior.STATE_DRAGGING -> {
                    }
                    BottomSheetBehavior.STATE_SETTLING -> {
                    }
                }            }

            override fun onSlide(bottomSheet: View, slideOffset: Float) {
                // React to dragging events
            }
        })
        back_button.setOnClickListener({
            super.onBackPressed()
        })
    }


    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        val latLng = LatLng(delivery!!.location.lat, delivery!!.location.lng)
        mMap!!.addMarker(MarkerOptions().position(latLng).title(delivery!!.location.address))
        val cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 12.5F)
        mMap?.animateCamera(cameraUpdate)
    }

    companion object {
        val gson = Gson()
        fun startAsModal(context: Context?, id: String = "", delivery: Delivery) =
            context?.startActivity(
                Intent(context, MapsActivity::class.java)
                    .putExtra(DELIVERY_ID, id)
                    .putExtra(DELIVERY_DATA, gson.toJson(delivery))
            )

    }

    protected override fun onResume() {
        super.onResume()
        behavior.state = BottomSheetBehavior.STATE_COLLAPSED
    }

    private fun updateDetials(delivery: Delivery)
    {
        deliveryDescription.text = "Description: " + delivery.description
        deliveryAddress.text = "Address: " + delivery.location.address
        deliveryimage.loadImage(delivery.imageUrl, R.drawable.no_image, R.drawable.no_image)
    }
}