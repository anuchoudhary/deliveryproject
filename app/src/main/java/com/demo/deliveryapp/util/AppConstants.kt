package com.demo.deliveryapp.util


val BASE_URL="https://demo3196012.mockable.io/"
const val SPLASH_VIEW = "SPLASH_VIEW"
const val BACK = "BACK"
const val BACKGROUND = "BACKGROUND"
const val MAIN_VIEW = "MAIN_VIEW"

const val DELIVERY_ID = "Delivery_id"
const val DELIVERY_DATA = "delivery_"