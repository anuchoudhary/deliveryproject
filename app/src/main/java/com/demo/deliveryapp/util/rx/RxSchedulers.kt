package com.demo.deliveryapp.util.rx

import io.reactivex.Scheduler

interface RxSchedulers {
    val io: Scheduler
    val mainThread: Scheduler
    val computation: Scheduler
}