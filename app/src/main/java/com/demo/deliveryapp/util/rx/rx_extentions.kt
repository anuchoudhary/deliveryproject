package com.demo.deliveryapp.util.rx

import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single

// Apply schedulers for I/O -> Main Thread
fun <T> Observable<T>.applyUiSchedulers(schedulers: RxSchedulers): Observable<T> {
    return subscribeOn(schedulers.io).observeOn(schedulers.mainThread)
}

// Apply schedulers for Computation -> Main Thread
fun <T> Observable<T>.applyComputationalSchedulers(schedulers: RxSchedulers): Observable<T> {
    return subscribeOn(schedulers.computation).observeOn(schedulers.mainThread)
}

// Apply schedulers for I/O -> Main Thread
fun <T> Single<T>.applyUiSchedulers(schedulers: RxSchedulers): Single<T> {
    return subscribeOn(schedulers.io).observeOn(schedulers.mainThread)
}

fun Completable.applyUiSchedulers(schedulers: RxSchedulers): Completable {
    return subscribeOn(schedulers.io).observeOn(schedulers.mainThread)
}

// Apply schedulers for Computation -> Main Thread
fun <T> Single<T>.applyComputationalSchedulers(schedulers: RxSchedulers): Single<T> {
    return subscribeOn(schedulers.computation).observeOn(schedulers.mainThread)
}