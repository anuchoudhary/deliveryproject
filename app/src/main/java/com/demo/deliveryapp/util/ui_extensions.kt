package com.demo.deliveryapp.util

import android.widget.ImageView
import androidx.annotation.DrawableRes
import com.squareup.picasso.Picasso


fun ImageView.loadImage(url: String?, @DrawableRes defaultImg: Int, @DrawableRes onErrorImg: Int) {
    Picasso.get()
        .load(url)
        .placeholder(defaultImg)
        .error(defaultImg)
        .into(this);
}